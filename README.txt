VERSIONS:
- tensorflow 1.4.0
- node 8.11.2

	
INSTRUCTIONS:
0. go to musicbox.js and look for a big 'email' comment. Write the address and the password of the email account that you want to use to send messages to users. 
1. run the backend in tools/backend.py with "py backend.py" and wait until it starts listening.
2. run the https server with "node musaical.js" and wait for the server to start listening.
3. go to https://localhost:4200 in your browser (the connection isnt private because we dont have an https cert).


NOTES:
- if server crashes it's probably because client chose a genre that is commented out in the backend, you can un-comment the networks you want to use in tools/backend.py lines 11-22.

- we've implemented midi-text and text-midi conversions in tools/midi_processing.py so you can have that!
 
- completion not yet implemented.

- https doesnt work so you will get a warning in the browser (we didnt make a cert because this project is too expensive to actually run).

- you cant use the server IP as a domain because the client script use localhost as a domain name.



CONTACT US: musicbox.ai@gmail.com