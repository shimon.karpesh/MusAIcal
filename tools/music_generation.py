import os
import tensorflow as tf
import numpy as np
import midi_processing
import time
import json
from tqdm import tqdm
from random import shuffle

'''
	this is what our model looks like:		
	
	output
	  |
	softmax
	  |
	dropout
	  |
	 LSTM
	  |
	dropout
	  |
	 LSTM	  
	  |
	input
'''

class Model:
	
	def __init__(self, model_path, data_path = None, sequence_size = None,  hidden_size = None, resolution = None, dropout_keep = None,
				 learning_rate = None, batch_size = None, explosion_bar = None, scope = None, device = None, bpm = None):
	
		if model_path[-1] != '/':
			model_path += '/'
		if data_path != None and data_path[-1] != '/':
			data_path += '/'
		parameters_path = model_path + 'parameters.json'
		self.checkpoint_path = model_path + 'model.ckpt'
		
		# checking parameters
		if os.path.isfile(parameters_path):
			parameters = json.load(open(parameters_path))
			# unmodifiable parameters			
			self.data_path = parameters['data_path']
			if data_path != None and self.data_path != data_path:
				raise ValueError('Cannot modify data_path, create a new model instead.')
				
			self.sequence_size = parameters['sequence_size']
			if sequence_size != None and self.sequence_size != sequence_size:
				raise ValueError('Cannot modify sequence_size, create a new model instead.')
			
			self.hidden_size = parameters['hidden_size']
			if hidden_size != None and self.hidden_size != hidden_size:
				raise ValueError('Cannot modify hidden_size, create a new model instead.')
				
			self.resolution = parameters['resolution']
			if resolution != None and self.resolution != resolution:
				raise ValueError('Cannot modify resolution, create a new model instead.')
			
			self.dropout_keep = parameters['dropout_keep']
			if dropout_keep != None and self.dropout_keep != dropout_keep:
				raise ValueError('Cannot modify dropout_keep, create a new model instead.')
				
			self.scope = parameters['scope']
			if scope != None and self.scope != scope:
				raise ValueError('Cannot modify scope, create a new model instead.')
			
			# modifiable parameters
			if learning_rate == None:
				self.learning_rate = parameters['learning_rate']
			else:
				self.learning_rate = learning_rate
				if learning_rate != parameters['learning_rate']:
					print('Learning rate modified:', learning_rate)
				
			if batch_size == None:
				self.batch_size = parameters['batch_size']
			else:
				self.batch_size = batch_size
				if batch_size != parameters['batch_size']:
					print('Batch size modified:', batch_size)
			
			# if there is a loss explosion in the training epoch, the last checkpoint will be loaded. the explosion bar is the minimal loss that the checkpoint will be loaded for.	
			if explosion_bar == None:
				self.explosion_bar = parameters['explosion_bar']
			else:
				self.explosion_bar = explosion_bar
				if explosion_bar != parameters['explosion_bar']:
					print('Explosion bar modified:', explosion_bar)	
		
			if device == None:
				self.device = parameters['device']
			else:
				self.device = device
				if device != parameters['device']:
					print('Device modified:', device)					
					
			if bpm == None:
				self.bpm = parameters['bpm']
			else:
				self.bpm = bpm
				if bpm != parameters['bpm']:
					print('BPM modified:', bpm)
					
		# if parameters file wasnt found  
		elif data_path == None or sequence_size == None or hidden_size == None or resolution == None or dropout_keep == None or learning_rate == None or batch_size == None or scope == None:  
			raise ValueError('parameters.json file not found, __init__ must recieve all necessary parameters.')
			
		else:
			self.data_path = data_path
			self.sequence_size = sequence_size
			self.hidden_size = hidden_size
			self.resolution = resolution
			self.dropout_keep = dropout_keep
			self.learning_rate = learning_rate
			self.batch_size = batch_size
			self.explosion_bar = explosion_bar
			self.scope = scope
			self.device = device
			self.bpm = bpm
		
		# writing json file
		if not os.path.exists(model_path):
			os.makedirs(model_path)		
		with open(parameters_path, 'w') as parameters_file:			
			json.dump({
				'data_path': self.data_path,
				'batch_size': self.batch_size,
				'sequence_size': self.sequence_size,
				'hidden_size': self.hidden_size,
				'resolution': self.resolution,
				'learning_rate': self.learning_rate,
				'dropout_keep': self.dropout_keep,
				'explosion_bar': self.explosion_bar,
				'scope': self.scope,
				'device': self.device,
				'bpm': self.bpm
			}, parameters_file)
			print('Parameters file updated.')
		
		
		# choosing between cpu and gpu
		if self.device.lower() == 'cpu':
			config = tf.ConfigProto(device_count = {'GPU': 0})			
			self.sess = tf.Session(config = config)
		elif self.device.lower() == 'gpu':
			self.sess = tf.Session()
		
		
		# getting things ready
		self.__gather_data()
		self.__build_model()
		print('Model', self.scope, 'built')		
		tf.global_variables_initializer().run(session=self.sess)
		print('Variables initialized.')
		
		
	    # loading checkpoint
		self.saver = tf.train.Saver(tf.trainable_variables())
		try:  
			self.saver.restore(self.sess, self.checkpoint_path)  # try to load weights if there are any.
		except:
			print('CHECKPOINT WARNING: If you are running only one network then the checkpoint was not found.')
		
		
	def __gather_data(self):
		if os.path.isfile(self.data_path + 'data.txt'):
			print('Reading data from ' + self.data_path + 'data.txt')
			data_file = open(self.data_path + 'data.txt', 'r')
			data = data_file.read()
			data_file.close()
		else:
			data = ''	
			print('Training files:')
			for filename in os.listdir(self.data_path):  # convert midi data to text.
				print(filename)
				data += midi_processing.midi_to_text(self.data_path + filename)
				data += '~800'
			data_file = open(self.data_path + 'data.txt', 'w')
			data_file.write(data)
			data_file.close()
			print('Data file saved at ' + self.data_path + 'data.txt')
		
		# dictionaries
		self.char_to_int = midi_processing.char_to_int()  
		self.int_to_char = midi_processing.int_to_char()
		
		length = len(data)
		# gather inputs and outputs, each input contains 160 characters and output contains the next single character.
		self.inputs = []
		self.outputs = []
		for i in range(0, length - self.sequence_size):  
			self.inputs.append([self.char_to_int[char] for char in data[i:i + self.sequence_size]])
			self.outputs.append(self.char_to_int[data[i + self.sequence_size]])
		n_patterns = len(self.inputs)
		print('Total patterns:', n_patterns)
		# the number of different characters in the input data, and the size of the output layer.
		self.vocab_size = max(self.outputs) + 1
		print('Vocab size:', self.vocab_size)
		
		# one hot encode
		self.dataX = np.zeros((n_patterns, self.sequence_size ,self.vocab_size), dtype=np.bool)
		self.dataY = np.zeros((n_patterns, self.vocab_size), dtype=np.float32)  
		
		for i, each in enumerate(self.outputs):
			self.dataY[i, each] = 1
		for i, sequence in enumerate(self.inputs):
			for t, val in enumerate(sequence):
				self.dataX[i, t, val] = 1 
		
		
	def __build_model(self):			
		with tf.variable_scope(self.scope):  
			self.X = tf.placeholder(tf.float32, [None, self.sequence_size, self.vocab_size])
			self.Y = tf.placeholder(tf.float32, [None, self.vocab_size])
			self.W = tf.Variable(tf.random_normal([self.hidden_size[1], self.vocab_size]))
			self.B = tf.Variable(tf.random_normal([self.vocab_size]))
			

			# first lstm layer with dropout
			lstm_cell_1 = tf.contrib.rnn.LSTMCell(self.hidden_size[0])  
			cell_1 = tf.nn.rnn_cell.DropoutWrapper(lstm_cell_1, input_keep_prob=1, output_keep_prob=self.dropout_keep[0])
			
			# second lstm layer
			lstm_cell_2 = tf.contrib.rnn.LSTMCell(self.hidden_size[1])
			cell_2 = tf.nn.rnn_cell.DropoutWrapper(lstm_cell_2, input_keep_prob=1, output_keep_prob=self.dropout_keep[1])
			
			lstm_cells = tf.contrib.rnn.MultiRNNCell(cells=[cell_1, cell_2])
			
			# rnn results		
			outputs, final_state = tf.nn.dynamic_rnn(lstm_cells, self.X, dtype=tf.float32)
			
			# softmax layer
			linear = tf.matmul(outputs[:, -1, :], self.W) + self.B
			self.probabilities = tf.nn.softmax(linear)
			
			loss = tf.nn.softmax_cross_entropy_with_logits(logits=linear, labels=self.Y) 
			self.loss = tf.reduce_mean(loss)  # getting average batch loss.
			
			self.optimizer = tf.train.RMSPropOptimizer(learning_rate=self.learning_rate).minimize(self.loss) 
			
			
	def train(self, n_epoch): 
		num_batches = int(len(self.dataX) / self.batch_size)
		s = np.arange(self.dataX.shape[0])
		min = float('Inf')
		
		for i in range(n_epoch):
			print('Epoch {0}/{1}:'.format(i+1, n_epoch))
			np.random.shuffle(s)
			self.dataX = self.dataX[s]
			self.dataY = self.dataY[s]
			count = 0
			j = 0
			avg = 0
			pbar = tqdm(range(num_batches), ncols = 100)  # loading bar.
			for k in pbar:
				train_batch, target_batch = self.dataX[count:count + self.batch_size], self.dataY[count:count + self.batch_size]
				_, cost = self.sess.run([self.optimizer, self.loss], feed_dict = {self.X: train_batch, self.Y: target_batch})
				avg = (avg * j + cost) / (j + 1)  # calculate average loss.
				pbar.set_description('Pattern: {} Avg loss: {:.4f}'.format(count, avg))
				pbar.refresh()
				count += self.batch_size
				j += 1
			if avg <= min:  # if average loss improved save checkpoint.
				print('Loss improved from', min, 'to', avg)
				min = avg
				self.saver.save(self.sess, self.checkpoint_path)
			else:
				print ('Loss did not improve')
			if self.explosion_bar and avg >= self.explosion_bar:  
				print('Loss explosion, restoring checkpoint.')
				self.saver.restore(self.sess, self.checkpoint_path)
				
				
	def generate(self, output_path, length):
		# generate random seed
		rand = np.random.randint(0, len(self.inputs)-1)
		pattern = self.dataX[rand].tolist()
		print('Generating:\n')
		output = ''
		start = time.time()

		for i in tqdm(range(length)):
			x = np.reshape(pattern, (1, self.sequence_size, self.vocab_size))
			prediction = self.sess.run(self.probabilities, feed_dict={self.X: x})
			
			index = np.argmax(prediction)  # get the index of the predicted note.
			output += self.int_to_char[index]	
				
			# add the new output to the inputs list, move the window so it will include the new output.
			new = [False] * self.vocab_size
			new[index] = True 
			pattern.append(new)
			pattern = pattern[1:len(pattern)]
			i += 1
			
		end = time.time()
		print('\nDone. Time elapsed:', (end - start))
		print(output)
		print(self.resolution, self.bpm)
		midi_processing.text_to_midi(output_path, output, self.bpm)
		
		
	def complete(self, output_path, length, piece):
		piece_end = piece[-160:]
		
		print('Completing piece:\n')
		completion = ''
		start = time.time()
		for i in tqdm(range(length)):
			x = np.reshape(pattern, (1, self.sequence_size, self.vocab_size))
			prediction = self.sess.run(self.probabilities, feed_dict={self.X: x})
			
			index = np.argmax(prediction)  # get the index of the predicted note.
			completion += self.int_to_char[index]	
				
			# add the new output to the inputs list, move the window so it will include the new output.
			new = [False] * self.vocab_size
			new[index] = True 
			pattern.append(new)
			pattern = pattern[1:len(pattern)]
			i += 1
			
		end = time.time()
		print('\nDone. Time elapsed:', (end - start))
		print(completion)
		midi_processing.text_to_midi(output_path, piece + completion, self.bpm)
		
		