import midi  # https://github.com/vishnubob/python-midi
import os
import bisect


# transforms a midi file into a text file.
def midi_to_text(path):
	dict = {i + 20 : chr(i) for i in range(1, 48)}
	dict.update({i + 10 : chr(i) for i in range(58, 99)})
	dict.update({0 : "~"})	
	
	change_ticks_for_resolution(path, 480)
	pattern = midi.read_midifile(path)
	pattern = merge_tracks(pattern)
	
	piece = ""
	open_notes = {}
	curr_tick = 0
	last_tick = 0
	for note in pattern[0][:-1]:
		curr_tick += note.tick		

		if curr_tick != last_tick:
			piece += dict[0] + str(curr_tick - last_tick)			
			
		if type(note) == midi.events.NoteOnEvent and note.velocity != 0:  # if its a note on event
			piece += dict[note.pitch]
			open_notes.update({note.pitch : curr_tick})
		elif (type(note) == midi.events.NoteOnEvent or type(note) == midi.events.NoteOffEvent) and note.pitch in open_notes:
			i = len(piece) - piece[::-1].find(dict[note.pitch]) - 1
			piece = piece[:i + 1] + str(curr_tick - open_notes[note.pitch]) + piece[i + 1:]
			del open_notes[note.pitch]
		
		last_tick = curr_tick
	
		
	return optimize(piece)


# shortens the piece string and removes contradictions.
def optimize(piece):
	i = 0
	while i < len(piece):
		if piece[i] == '~':
			num = "0"
			j = i
			times = 0
			while j < len(piece) and piece[j] == '~':
				times += 1
				j += 1
				curr_num = "0"
				while j < len(piece) and piece[j] in "0123456789":
					curr_num += piece[j]
					j += 1
				num = str(int(num) + int(curr_num))
			if times > 1:
				piece = piece[:i + 1] + num + piece[j:]
				
		i += 1	
	
	i = 0
	while i < len(piece):
		if piece[i] not in "0123456789" and piece[i + 1] not in "0123456789":
			piece = piece[:i] + piece[i + 1:]
		else:
			i += 1	
	
	return piece
	
	
# transfers a string into a midi file.
def text_to_midi(path, piece, bpm):
	dict = {chr(i) : i + 20 for i in range(1, 48)}
	dict.update({chr(i) : i + 10 for i in range(58, 99)})
	dict.update({"~" : 0})
	
	pattern = midi.Pattern(resolution=480)
	
	# setting tempo
	tempo_track = midi.Track()
	tempo_track.append(midi.SetTempoEvent(bpm=bpm))
	tempo_track.append(midi.EndOfTrackEvent())
	pattern.append(tempo_track)
	
	track = midi.Track() 
			
	notes_list = []
	num = ""
	i = 0
	
	while i < len(piece):
		num = ""
		note = piece[i]
		i += 1
		while i < len(piece) and piece[i] in "0123456789":
			num += piece[i]
			i += 1
		if note in dict and num != "" and int(num) < 10000:
			notes_list.append((dict[note], int(num)))
			
	curr_tick = 0
	last_tick = 0
	open_notes = []
	for i in notes_list:
		if i[0] == 0:
			curr_tick += i[1]
			j = 0
			while j < len(open_notes):
				if curr_tick >= open_notes[j][1]:
					track.append(midi.NoteOnEvent(tick = open_notes[j][1] - last_tick, pitch = open_notes[j][0], velocity = 0))
					last_tick = open_notes[j][1]
					del open_notes[j]
				else:
					j += 1
			
		else:
			track.append(midi.NoteOnEvent(tick = curr_tick - last_tick, pitch = i[0], velocity = 70))
			last_tick = curr_tick
			open_notes.append((i[0], curr_tick + i[1]))	
			open_notes = sorted(open_notes, key = lambda note: note[1])
			
			
	for j in open_notes:
		track.append(midi.NoteOffEvent(tick = j[1] - last_tick, pitch = j[0]))
		last_tick = j[1]
			
	track.append(midi.EndOfTrackEvent(tick=1))
	pattern.append(track)
	midi.write_midifile(path, pattern)
	print("generated file saved at", path)
	print(midi_to_text(path))

		
# returns a midi pattern where the tracks are merged into one.
def merge_tracks(pattern):
	track = midi.Track()
	notes = []
	
	for t in pattern:
		sum_ticks = 0
		for note in t:
			sum_ticks += note.tick
			if (type(note) == midi.events.NoteOnEvent) or (type(note) == midi.events.NoteOffEvent) or (type(note) == midi.events.SetTempoEvent):
				notes.append((sum_ticks, note))
		
	notes = sorted(notes, key = lambda note: note[0])  # notes contains all the note/tempo events in the midi file. 
	
	# merging the tracks 
	notes[0][1].tick = 0
	track.append(notes[0][1])
	for i in range(1, len(notes)):
		notes[i][1].tick = notes[i][0] - notes[i - 1][0]
		track.append(notes[i][1])
	
	track.append(midi.EndOfTrackEvent(tick=1))
	merge = midi.Pattern(resolution=pattern.resolution)
	merge.append(track)
	return merge


# gets a path and a number and adds the number to all the pitches in the file.
def transpose(path, n):
	pattern = midi.read_midifile(path)
	
	for track in pattern:
		for note in track:
			if type(note) == midi.events.NoteOnEvent or type(note) == midi.events.NoteOnEvent:
				note.pitch += n
				if note.pitch < 21 or note.pitch > 108:
					track.remove(note)
				
	midi.write_midifile(path, pattern)


# multiplies the ticks by a value so the piece will sound like a piece in different resolution.	
def change_ticks_for_resolution(path, resolution):
	pattern = midi.read_midifile(path)
	pattern = merge_tracks(pattern)
	
	original = pattern.resolution
	mul = resolution / original
	
	for t in pattern:
		for note in t:
			note.tick = int(note.tick * mul)
			
	midi.write_midifile(path, pattern)
	

# multiplies the ticks by a value so the piece will sound like a piece in different tempo.
def change_ticks_for_tempo(path, target_tempo):
	pattern = midi.read_midifile(path)
	pattern = merge_tracks(pattern)
	
	multiplier = 1
	
	for track in pattern:
		for note in track:
			note.tick = int(multiplier * note.tick)
			if type(note) == midi.events.SetTempoEvent:
				multiplier = note.get_bpm() / target_tempo
	
	midi.write_midifile(path, pattern)
	
	
def char_to_int():
	dict = {chr(i) : i for i in range(1, 99)}
	dict.update({'~' : 0})
	return dict
	

def int_to_char():
	dict = {i : chr(i) for i in range(1, 99)}
	dict.update({0 : '~'})
	return dict