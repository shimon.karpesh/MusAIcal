import music_generation
import midi_processing as mp
from threading import Thread
import socket
import queue
import os
import base64
from msvcrt import getch


models = {
	'generate': {
		'classical': {
			'bach': music_generation.Model(model_path='models/classical/bach', scope='generate_classical_bach', device='cpu'),
			'mozart': music_generation.Model(model_path ='models/classical/mozart', scope='generate_classical_mozart', device='cpu')
		},
		'ragtime': {
			'scott_joplin': music_generation.Model(model_path = 'models/ragtime/scott_joplin', scope = 'generate_ragtime_scott_joplin', device='cpu'),
			'james_scott': music_generation.Model(model_path = 'models/ragtime/james_scott', scope = 'generate_ragtime_james_scott', device='cpu')
		}
	}
}



tempos = {
	'music_generation' : {
		'mozart' : 100,
		'bach' : 100,
		'scott_joplin' : 480,
		'james_scott' : 120 
	}, 
	
	'music_completion' : {
		'mozart' : 100,
		'bach' : 100,
		'scott_joplin' : 480,
		'james_scott' : 120 
	}
}


q = queue.Queue()
checkQ = queue.Queue()

IP = '127.0.0.1'
port = 8282

soc = socket.socket()
soc.bind((IP, port))
print('Listening...')
soc.listen(1)
client_soc, client_address = soc.accept()
print('Server connected')


def listen():
	while True:
		request = str(client_soc.recv(4096), 'utf-8')
		print('request added: ' + request)
		
		if request.split('$')[0] == 'check':  # if it's a check request.
			checkQ.put(request.split('$')[1])
		else:
			q.put(request)


# update message example: "update:81dbe011-d69c-d476-8a36-0c6c8ca2fa00 1
# 						   fd284edf-f587-7dd4-e58b-2cbaac0bad02 2"
def update_message(curr_request):
	msg = 'update:'
	msg += curr_request.split(' ')[3] + ' ' + 'generating\n'
	for index, request in enumerate(list(q.queue)):
		msg += request.split(' ')[3] + ' ' + str(index + 1) + '\n'
	return msg[:-1]  # remove last \n
	
		
		
# generation request example: "generate classic bach 81dbe011-d69c-d476-8a36-0c6c8ca2fa00 mymail@mail.com"
# completion request example: "complete classic bach 81dbe011-d69c-d476-8a36-0c6c8ca2fa00 mymail@mail.com 3"
def handle_request():
	while True:
		request = q.get(block=True)
		print('Sending update message')
		client_soc.send(update_message(request).encode('utf-8'))
		
		print('Processing request: ' + request)
		request_parts = request.split(' ')
		
		if request_parts[0] == 'generate':
			filepath = '../public/midi/' + request_parts[3] + '.mid'
			models['generate'][request_parts[1]][request_parts[2]].generate(filepath, 1500)
			# send back guid and email, the server will send the result link to the email.
			client_soc.send(('result:' + ' '.join((request_parts[3], request_parts[4], request_parts[2]))).encode('utf-8'))
			
		elif request_parts[0] == 'complete':
			filepath = '../public/uploads/' + request_parts[3] + '.mid'
			piece = midi.read_midifile(filepath)
			models['generate'][request_parts[1]][request_parts[2]].complete(filepath, 1500, piece)
			client_soc.send(('result:' + ' '.join(request_parts[3], request_parts[4], request_parts[2]).encode('utf-8')))

			

# completion request example: "complete classic bach 81dbe011-d69c-d476-8a36-0c6c8ca2fa00 mymail@mail.com 3"			
def check():
	while True:
		check_request = checkQ.get(block=True)
		print('Processing check request: ' + check_request)
		request_parts = check_request.split(' ')
		filepath = '../public/uploads/' + request_parts[3] + '.mid'
		text = ''
		if request_parts[0] == 'complete':			
			try:
				mp.transpose(filepath, int(request_parts[5]))
				print('hello1')
				mp.change_ticks_for_tempo(filepath, tempos['music_completion'][request_parts[2]])
				print('hello2')
				text = mp.midi_to_text(filepath)
				print('hello3')
			except:
				result = 'error'
				
			if len(text) >= 160:
				result = 'ok'
			else:
				result = 'error'
				
			# tell the user if the file is valid.
			client_soc.send(('check:' + request_parts[3] + ' ' + result).encode('utf-8'))
			
			if result == 'ok':
				request = ' '.join(request_parts[:-2]) + ' ' + text
				q.put(request)		


def main():
	t1 = Thread(target=listen)
	t2 = Thread(target=handle_request)
	t3 = Thread(target=check)
	
	t1.daemon = True
	t2.daemon = True
	t3.daemon = True
	
	t1.start()
	t2.start()
	t3.start()
	
	while True:
		key = ord(getch())
		if key == 27: #ESC
			soc.close()
			break

	
if __name__ == '__main__':
    main()
