#import model_normal as model
import model
import os

model2 = model.Model(model_path = 'models/classical/mozart/mozart6',
					data_path = 'data/classical/mozart',
					sequence_size = 300,
					hidden_size = [128, 128],
					dropout_keep = [0.8, 0.8],
					batch_size = 400,
					resolution = 480,
					scope = 'generate_classical_mozart',
					learning_rate = 0.001,
					bpm = 100,
					device = 'gpu')

					
#model = model.Model(model_path = 'models/classical/mozart/mozart5 - 0.2 loss', device = 'cpu')
 
#model2.train(15000)
#model.generate('moz.mid', 10000)
model2.generate('moz2.mid', 1000)


