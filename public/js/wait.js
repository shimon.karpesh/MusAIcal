var socket = io('https://localhost:4200');
var urlParts = window.location.href.split('/');
var tempo = urlParts[urlParts.length - 1];
var artist = urlParts[urlParts.length - 2];
var guid = urlParts[urlParts.length - 3];

socket.emit('join', guid);

socket.on('update', (index) => {
	if (index == 'generating')
		document.getElementById('wait-text').innerHTML = 'Generating...';
	else
		document.getElementById('wait-text').innerHTML = 'Your position in the queue: ' + index;
});

socket.on('ready', () => {
	socket.disconnect();
	window.location.replace('https://localhost:4200/result/' + guid + '/' + artist + '/' + tempo);		
});

socket.on('check', (result) => {
	if (result == 'ok')
		document.getElementById('wait-text').innerHTML = 'File accepted. Hold on...';
	else {
		alert('Woops!\nSomething is wrong with your midi file. Redirecting...');
		window.location.replace('https://localhost:4200/music_completion');	
	}
});