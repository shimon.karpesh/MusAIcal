function getArtists() {
	var classical = ['Mozart', 'Bach'];
	var ragtime = ['Scott Joplin', 'James Scott'];

	var genreSelection = document.getElementById('genre');
	var genre = genreSelection.options[genreSelection.selectedIndex].value;
	var artistSelection = document.getElementById('artist');

	artistSelection.length = 0;	// clearing artist selection box.

	// adding new options.
	switch (genre) {
	case 'classical':
		for (var i = 0; i < classical.length; i++) { 
			var option = document.createElement('option');
			option.value = classical[i].toLowerCase().replace(' ', '_');
			option.text = classical[i];
			artistSelection.add(option);
		}							
		break;
		
	case 'ragtime':
		for (var i = 0; i < ragtime.length; i++) {
			var option = document.createElement('option');
			option.value = ragtime[i].toLowerCase().replace(' ', '_');
			option.text = ragtime[i];
			artistSelection.add(option);
		}
		break;
	}

	artistSelection.disabled = false;
}


function generate_guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


$(document).ready(function(){
	$('#music_completion').submit(function(eventObj) {
		$('<input />').attr('type', 'hidden')
			.attr('name', 'guid')
			.attr('value', generate_guid())
			.appendTo('#music_completion');
		return true;
	});
});