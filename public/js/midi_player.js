var player;
var songid = 0;
var urlParts = window.location.href.split('/');
var tempo = urlParts[urlParts.length - 1];
var artist = urlParts[urlParts.length - 2];
var guid = urlParts[urlParts.length - 3];
var song = 'https://localhost:4200/midi/' + guid + '.mid';
var playing = true;

MIDI.Player.BPM = tempo;

function pausePlayStop(stop) {
	var d = document.getElementById("pausePlayStop");
	if (stop) {
		d.src = "/images/play.png";
		MIDI.Player.stop();
		playing = false;
	} else if (playing) {
		d.src = "/images/play.png";
		MIDI.Player.pause(true);
		playing = false;
	} else {
		d.src = "/images/pause.png";
		MIDI.Player.start();
		playing = true;
	}
}

	
function MIDIPlayerPercentage(player) {
	// update the timestamp
	var time = document.getElementById("time");
	var capsule = document.getElementById("capsule");
	var timeCursor = document.getElementById("cursor");

	eventjs.add(capsule, "drag", function(event, self) {
		eventjs.cancel(event);
		player.currentTime = (self.x) / document.getElementById("capsule").offsetWidth * player.endTime;
		if (player.currentTime < 0) player.currentTime = 0;
		if (player.currentTime > player.endTime) player.currentTime = player.endTime;
		if (self.state === "down") player.pause(true);
		else if (self.state === "up") {
			if (playing) {
				player.start();
			}
		}			
	});
	
	function timeFormatting(n) {
		var minutes = n / 60 >> 0;
		var seconds = String(n - (minutes * 60) >> 0);
		if (seconds.length == 1) seconds = "0" + seconds;
		return minutes + ":" + seconds;
	}
	
	player.setAnimation(function(data, element) {
		var percent = data.now / data.end;
		var now = data.now >> 0; // where we are now
		var end = data.end >> 0; // end of song
		// display the information to the user
		timeCursor.style.width = (percent * 100) + "%";
		time.innerHTML = timeFormatting(now) + " / " + timeFormatting(end);
	});
}


eventjs.add(window, "load", function(event) {
	MIDI.loadPlugin({
		soundfontUrl: "/soundfont/",
		onsuccess: function() {
			// setting up the midi player
			player = MIDI.Player;
			player.loadFile(song, player.start);
			MIDIPlayerPercentage(player);
			// setting up the download link
			var downloadLink = document.getElementById("download");
			downloadLink.href = song;
		}
	});
	document.getElementById("showcase").style = "min-height: 500px; background: url('../../../images/" + artist + ".jpg') no-repeat; background-position: center; background-size: cover; background-attachment: fixed; position: relative; z-index: -999; color: #FFFFFF;";
});
