// requirements
var fs = require('fs'),
	check = require('syntax-error'),
	express = require('express'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	https = require('https'),
	net = require('net'),
	fileUpload = require('express-fileupload'),
	nodemailer = require('nodemailer');

	
// syntax error checking (from stackoverflow)
var file = __dirname + '/musicbox.js';
var src = fs.readFileSync(file);
var err = check(src, file);
if (err) {
    console.error('ERROR DETECTED' + Array(62).join('!'));
    console.error(err);
    console.error(Array(76).join('-'));
}


// app setup
var app = express();
app.use(session({   
	secret: 'MUSICAL_POTATOES',
	cookie: { maxAge: 1000 * 60 * 60 * 24 }
}));
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));  // enable url parameters.
app.use(express.static('public'));


// server setup
var our_domain = "https://localhost:4200"
var options = {
	key: fs.readFileSync('key.pem'),
	cert: fs.readFileSync('cert.pem')
};
var server = https.createServer(options, app);
server.listen(4200, () => {
	console.log('listening on port 4200');
});


// connection to backend
var backendSoc = new net.Socket();
backendSoc.connect(8282, '127.0.0.1', function() {
	console.log('Connected to backend');
});


// socket.io setup 
io = require('socket.io')(server);

/******************************               EMAIL                 ************************************/
// use your email here
var our_email = 'yourEmail@gmail.com';
var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: our_email,
		pass: 'yourPassword'  // use your password
	}
});


// from stack overflow
function isEmailValid(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


var available_parameters = {
	'music_generation' : {
		'classical' : ['mozart', 'bach'],
		'ragtime' : ['scott_joplin', 'james_scott']
	},
	
	'music_completion' : {
		'classical' : ['mozart', 'bach'],
		'ragtime' : ['scott_joplin', 'james_scott']
	}
}


var tempos = {
	'mozart' : 100,
	'bach' : 100,
	'scott_joplin' : 120,
	'james_scott' : 120 
};


var keys = {
	'A major OR F#/G minor': 1
}



// routes handeling


// root.
app.get('/', function(req, res) {
    res.redirect('/home');
});


// home page.
app.get('/home', function(req, res) {
	fs.readFile('views/home.html', function(e, data) {
		if (e) return res.status(500).json(e);
        res.send(data.toString());
    }); 
});


// music generation tool page.
app.route('/music_generation')
.get(function(req, res) {
	fs.readFile('views/music_generation.html', function(e, data){
		if (e) return res.status(500).json(e);
        res.send(data.toString());
    }); 
})
.post(function(req, res) {
	var genre = req.body.genre;
	var artist = req.body.artist;
	var side = req.body.side;
	var guid = req.body.guid;
	var email = req.body.email;
	console.log('Received music generation request: genre = ' + genre + ', artist = ' + artist  + ', guid = ' + guid + ', email = ' + email);
	
	// if the user tried to alter the values in the post request
	if (!(genre in available_parameters['music_generation']) || !(available_parameters['music_generation'][genre].includes(artist))) {
		return res.status(400).send('nice try');
	}
	
	backendSoc.write('generate ' + genre + ' ' + artist + ' ' + guid + ' ' + email + ' ' + Buffer.from("Hello World").toString('base64'));
	res.redirect(303, '/result/' + guid + '/' + artist + '/' + tempos[artist]);
});


// music completion tool page.
app.route('/music_completion')
.get(function(req, res) {
	fs.readFile('views/music_completion.html', function(e, data) {
		if (e) return res.status(500).json(e);
        res.send(data.toString());
    }); 
})
.post(function(req, res) {  // need to implement file upload here
	var genre = req.body.genre;
	var artist = req.body.artist;
	var guid = req.body.guid;
	var key = req.body.key;
	var email = req.body.email;
	console.log('Received music completion request: genre = ' + genre + ', artist = ' + artist  + ', guid = ' + guid + ', key = ' + key + ', email = ' + email);
	
	// if the user tried to alter the values in the post request
	if (!(genre in available_parameters['music_completion']) || !(available_parameters['music_completion'][genre].includes(artist)) || !(key in keys)) {	
		return res.status(400).send('nice try');
	}
	
	if (!req.files) return res.status(400).send('No files were uploaded.');
	
	req.files.sampleFile.mv(__dirname + '/public/uploads/' + guid + '.mid', function(e) {
		if (err) return res.status(500).json(e);
	});

	
	console.log('File uploaded: ' + req.files.sampleFile.name);
	
	backendSoc.write('check$complete ' + genre + ' ' + artist + ' ' + guid + ' ' + email + ' ' + keys[key]);
	res.redirect(303, '/result/' + guid + '/' + artist + '/' + tempos[artist]);
});


// result page.
app.get('/result/:guid/:artist/:tempo', function(req, res) {
	if (fs.existsSync('public/midi/' + req.params.guid + '.mid')) {
		fs.readFile('views/result.html', function(e, data) {
			if (e) return res.status(500).json(e);
			res.send(data.toString());
		});	
	} else {
		fs.readFile('views/wait.html', function(e, data) {
			if (e) return res.status(500).json(e);
			res.send(data.toString());
		});	
	}
});


// backend sends a message when the result is ready. The message contains: "{guid} {email} {artist}"
backendSoc.on('data', function(data) {
	data = data.toString('utf-8');
	console.log(data);
	var header = data.split(':')[0];
	data = data.split(':')[1];
	
	if (header == 'result') {
		var guid = data.split(' ')[0];
		var receiver = data.split(' ')[1];
		var artist = data.split(' ')[2];
		io.in(guid).emit('ready');
		
		if (isEmailValid(receiver)) {
			var mailOptions = {
				from: our_email,
				to: receiver,
				subject: 'Your result is ready!',
				text: 'Your result is availabe at the link: ' + our_domain + '/result/' + guid + '/' + artist + '/' + tempos[artist]
			};	
		
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) return console.log(error);
				console.log('Message sent: %s', receiver);
			});
		}	
	} else if (header == 'update') {
		var parts = data.split('\n');
		for (var i in parts) {
			var guid = parts[i].split(' ')[0];
			var index = parts[i].split(' ')[1];	
			io.in(guid).emit('update', index);
		}
		console.log('Sent update messages');
	} else if (header == 'check') {
		var guid = data.split(' ')[0];
		var result = data.split(' ')[1];
		io.in(guid).emit('check', result);
		console.log('Sent check messages');
	}
});


// socket io
io.on('connection', (clientSoc) => {	
	clientSoc.on('join', (data) => {
		console.log(data + ' connected');
		clientSoc.join(data);
	});
	
	clientSoc.on('disconnect', () => {		
		clientSoc.disconnect();
	});	
});