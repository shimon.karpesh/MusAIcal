import time
import os


path = 'public/generated/'
max_size = 500000  # just in case we someday manage to run this on a better machine (the model cant generate 1/2 gb in 36 hours on our pc)
ttl = 36 * 60 * 60

print('I\'m the destroyer of midi')

while True:
	sum = 0
	for filename in os.listdir(path):
		if time.time() - os.path.getmtime(path + filename) > ttl:
			os.remove(path + filename)
		else:
			sum += os.path.getsize(path + filename) / 1000
			
		
	if sum >= max_size:
		files = [(filname, os.path.getmtime(path + filename)) for filename in os.listdir(path)]
		files = sorted(files, key = lambda file: file[1])
		
		for i in range(5):
			print('Prepare to be destroyed', files[-1][0])
			os.remove(path + files[-1][0])
			del files[-1]
	
	time.sleep(60)
	